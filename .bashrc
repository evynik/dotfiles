if [ -f /etc/bashrc ]; then
. /etc/bashrc
fi

source /Users/nikoloudaki/miniconda3/etc/profile.d/conda.sh

PS1="\[\033[01;32m\[\u\[\033[01;31m\]@\[\033[01;32m\]\h \W]\$ \[\033[00m\]"
alias ls='ls --color'
alias lt='la -hltr'
alias lr='ls -hlatr'
alias ll='ls -hl'
alias la='ls -hls'
alias resource='source ~./bashrc'
alias dsconda='conda activate ds'
alias py='python3'
alias lyon='ssh -L 9500:localhost:7400 -XY evnikolo@cca.in2p3.fr'


alias config='/usr/bin/git --git-dir=/Users/nikoloudaki/.cfg/ --work-tree=/Users/nikoloudaki'
